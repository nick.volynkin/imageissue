## Assignment #2: My Future Job... I Think?

### Overview:
This assignment is a quick research task to help you understand the web development landscape. 

### Instructions:

1. Read through the ["So You Wanna be a Web Developer..."](README.md) chapter of Pre-Work.

2. Then visit a job site like [Indeed](http://www.indeed.com/) or [Dice](http://www.dice.com/).

3. Search for any of the following job positions:

	* Web Developer

	* Frontend Web Developer

	* Backend Developer

	* Javascript Developer

	* Node Developer

4. Read the job postings for at least 10 of the listings. As you are reading, take note of at least one technology, tool, or term you are unfamiliar with from each listing. (eg: "Angular" or "API"). See the example below:

	![Job Description](Images/JobDescription.png)

5. Then, search online for a definition or description of that technology or term. Try to understand what role it plays in the context of how web applications were described in that chapter.

6. Open a Word or Text file and paste your description for each of the technologies you researched. You should have a total of ten descriptions listed. 

### Note:
  * Don't overthink this. We just want you to begin contextualizing the technologies you'll be learning about in class and beyond. 

-------

### Copyright 
Coding Boot Camp (C) 2016. All Rights Reserved.
